package pl.codementors.zoo.animals;

/**
 * Just an animal.
 *
 * @author psysiu
 */
public abstract class Animal implements Comparable<Animal> {


    /**
     * Animal name.
     */
    private String name;

    public Animal(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Animal [name = " + name + "]";
    }


    @Override
    public boolean equals(Object a) {
        if (this == a) {
            return true;
        }
        if (!(a instanceof Animal)) {
            return false;
        }
        Animal animal = (Animal) a;
        return name.equals(animal.getName());
    }

    @Override
    public int hashCode() {
        return 37 * getName().hashCode();
    }

    @Override
    public int compareTo(Animal o) {
        int ret = name.compareTo(o.getName());
        if (ret == 0) {
            return getClass().getCanonicalName().compareTo(o.getClass().getCanonicalName());
        }
        return ret;
    }
}
