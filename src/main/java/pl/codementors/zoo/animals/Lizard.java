package pl.codementors.zoo.animals;

/**
 * Lizard with some scales.
 *
 * @author psysiu
 */
public class Lizard extends Animal {

    /**
     * Color of the animal scales.
     */
    private String scalesColor;

    public Lizard(String name, String scalesColor) {
        super(name);
        this.scalesColor = scalesColor;
    }

    public String getScalesColor() {
        return scalesColor;
    }

    public void setScalesColor(String scalesColor) {
        this.scalesColor = scalesColor;
    }

    @Override
    public String toString() {
        return "Lizard [name =" + super.toString() + scalesColor + "]";
    }

    @Override
    public int hashCode(){
        return getScalesColor().hashCode();
    }
    @Override
    public boolean equals(Object l) {
        if (this == l) {
            return true;
        }
        if (!(l instanceof Lizard)) {
            return false;
        }
        Lizard lizard = (Lizard) l;
        return getName().equals(lizard.getName()) && getScalesColor().equals(lizard.getScalesColor());
    }

    @Override
    public int compareTo(Animal o) {
        int ret = super.compareTo(o);
        if (ret == 0) {
            if (o instanceof Lizard) {
                return scalesColor.compareTo(((Lizard)o).getScalesColor());
            }
        }
        return ret;
    }
}
