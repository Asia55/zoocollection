package pl.codementors.zoo.animals;

/**
 * Created by student on 11.06.17.
 */
public class Tiger extends Mammal {

    public Tiger(String name, String furColor) {
        super(name, furColor);
    }

    @Override
    public String toString() {
        return "Tiger [" + super.toString() + "]";

    }

    @Override
    public int hashCode() {
        return getFurColor().hashCode();
    }

    @Override
    public boolean equals(Object t) {
        if (this == t) {
            return true;
        }
        if (!(t instanceof Tiger)) {
            return false;
        }
        Tiger tiger = (Tiger) t;
        return getName().equals(tiger.getName()) && getFurColor().equals(tiger.getFurColor());
    }


}
