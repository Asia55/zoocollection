package pl.codementors.zoo.animals;

import java.util.Comparator;

/**
 * Created by student on 11.06.17.
 */
public class MapComparator implements Comparator<String>{

    @Override
    public int compare(String a1, String a2) {
        return (-1)*a1.compareTo(a2);
    }
}
