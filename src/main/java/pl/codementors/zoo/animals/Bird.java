package pl.codementors.zoo.animals;

/**
 * Just a bird with feathers.
 *
 * @author psysiu
 */
public class Bird extends Animal {

    /**
     * Color of the bird feathers.
     */
    private String featherColor;

    public Bird(String name, String featherColor) {
        super(name);
        this.featherColor = featherColor;
    }

    public String getFeatherColor() {
        return featherColor;
    }

    public void setFeatherColor(String featherColor) {
        this.featherColor = featherColor;
    }

    @Override
    public String toString() {
        return "Bird [ name =" + super.toString() + featherColor + "]";
    }

    @Override
    public int hashCode() {
        return 37 * getFeatherColor().hashCode();
    }

    @Override
    public boolean equals(Object b) {
        if (this == b) {
            return true;
        }
        if (!(b instanceof Bird)) {
            return false;
        }
        Bird bird = (Bird) b;
        return getName().equals(bird.getName()) && getFeatherColor().equals(bird.getFeatherColor());
    }

        @Override
    public int compareTo(Animal o) {
        int ret = super.compareTo(o);
        if (ret == 0) {
            if (o instanceof Bird) {
                return featherColor.compareTo(((Bird)o).getFeatherColor());
            }
        }
        return ret;
    }
}
