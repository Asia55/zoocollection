package pl.codementors.zoo.animals;

import java.util.Comparator;

/**
 * Created by student on 11.06.17.
 */
public class AnimalComparator implements Comparator<Animal> {

    @Override
    public int compare(Animal a1, Animal a2) {
        String class1 = a1.getClass().getSimpleName();
        String class2 = a2.getClass().getSimpleName();
        int ret = class1.compareTo(class2);
        if (ret == 0){
            ret = a1.getName().compareTo(a2.getName());
        }
        return ret;
    }

}
