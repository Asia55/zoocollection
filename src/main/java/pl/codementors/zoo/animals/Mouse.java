package pl.codementors.zoo.animals;

/**
 * Created by student on 11.06.17.
 */
public class Mouse extends Mammal {

    public Mouse(String name, String furColor) {
        super(name, furColor);
    }

    @Override
    public String toString() {
        return "Mouse [" + super.toString() + "]";

    }

    @Override
    public int hashCode(){
        return getFurColor().hashCode();
    }
    @Override
    public boolean equals(Object m) {
        if (this == m) {
            return true;
        }
        if (!(m instanceof Mouse)) {
            return false;
        }
        Mouse mouse = (Mouse) m;
        return getName().equals(mouse.getName()) & getFurColor().equals(mouse.getFurColor());
    }


}
