package pl.codementors.zoo.animals;

import java.util.Objects;

/**
 * Created by student on 15.06.17.
 */
public class Owner{

    private String name;

    public Owner(){

    }

    public Owner(String name) {
        this.name = name;
    }

    public String getOwnerName() {
        return name;
    }

    public void setOwnerName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Owner owner = (Owner) o;

        return name != null ? name.equals(owner.name) : owner.name == null;
    }

    @Override
    public int hashCode() {
        return name != null ? name.hashCode() : 0;
    }

    public enum Type { BREEDER, COLLECTOR }

    private Type type;

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

}
