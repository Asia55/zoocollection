package pl.codementors.zoo;

import pl.codementors.zoo.animals.*;
import sun.reflect.generics.tree.Tree;

import java.io.IOException;
import java.io.InputStream;
import java.util.*;

/**
 * Application main class.
 *
 * @author psysiu
 */
public class ZooMain {

    /**
     * Application starting method.
     *
     * @param args Application starting params.
     */
    public static void main(String[] args) {

        List<Animal> animals = new ArrayList<>();
        Map<Owner, List<Animal>> animalAndOwner = new HashMap<>();


        try (InputStream is = ZooMain.class.getResourceAsStream("/pl/codementors/zoo/input/animals.txt");
             Scanner scanner = new Scanner(is)) {
            while (scanner.hasNext()) {
                String ownerName = scanner.next();
                String ownerType = scanner.next();
                String type = scanner.next();
                String name = scanner.next();
                String color = scanner.next();

                Owner.Type typ = Owner.Type.valueOf(ownerType);


                Owner owner = new Owner(ownerName);
                owner.setType(typ);


                if (!animalAndOwner.containsKey(owner)) {
                    animalAndOwner.put(owner, new ArrayList<>());
                }

                switch (type) {
                    case "Bird": {
                        animals.add(new Bird(name, color));
                        animalAndOwner.get(owner).add(new Bird(name, color));
                        break;
                    }
                    case "Lizard": {
                        animals.add(new Lizard(name, color));
                        animalAndOwner.get(owner).add(new Lizard(name, color));
                        break;
                    }
                    case "Mammal": {
                        animals.add(new Mammal(name, color));
                        animalAndOwner.get(owner).add(new Mammal(name, color));
                        break;
                    }
                    case "Tiger": {
                        animals.add(new Tiger(name, color));
                        animalAndOwner.get(owner).add(new Tiger(name, color));
                        break;
                    }
                    case "Mouse": {
                        animals.add(new Mouse(name, color));
                        animalAndOwner.get(owner).add(new Tiger(name, color));
                        break;
                    }
                }
            }
        } catch (IOException ex) {
            System.err.println(ex.getMessage());
            ex.printStackTrace();
        }
        Collections.sort(animals, new AnimalComparator());
        print(animals);
        System.out.println("--------------------------------------------");

        Set<Animal> animalSet = new TreeSet<>(new AnimalComparator());
        animalSet.addAll(animals);
        print(animalSet);
        System.out.println(animalSet.size());
        printAll(animals);

        Set<Mammal> mammalSet = new HashSet<>();
        List<Bird> birdList = new ArrayList<>();
        Collection<Lizard> lizardCollection = new TreeSet<>();

        for (Animal a : animalSet) {
            if (a instanceof Mammal) {
                mammalSet.add((Mammal) a);
            } else if (a instanceof Bird) {
                birdList.add((Bird) a);
            } else if (a instanceof Lizard) {
                lizardCollection.add((Lizard) a);
            }
        }
//        printAll(mammalSet);
//        printAll(birdList);
//        printAll(lizardCollection);
//        printMap(animalAndOwner);

//        Map<String, Animal> animalMap = new TreeMap<>(new MapComparator());
//        animalMap.putAll(animalAndOwner);
//        printMap(animalMap);
        printMap(animalAndOwner);
    }


    public static void printAll(Collection<? extends Animal> animals) {
        for (Animal animal : animals) {
            System.out.println(animal);
        }
    }

    public static void print(Collection<Animal> animals) {
        for (Animal animal : animals) {
            System.out.println(animal);
        }
    }

    public static void printMap(Map<Owner, List<Animal>> animals) {
        for (Owner key : animals.keySet()) {
            System.out.println(" ");
            System.out.print(key.getOwnerName() + " " + key.getType() + " " + "->" + " ");

            for (Animal animal : animals.get(key)) {
                System.out.print("[" + animal + "]");
            }
        }
    }
}
